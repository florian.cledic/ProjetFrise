package fr.uvsq.projetfrise.vue;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.TreeSet;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.toedter.calendar.JYearChooser;

import fr.uvsq.projetfrise.controleur.Controleur;
import fr.uvsq.projetfrise.modele.Date;
import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Frise;
import fr.uvsq.projetfrise.modele.Importance;
import fr.uvsq.projetfrise.outils.Data;
import fr.uvsq.projetfrise.outils.ExceptionDate;
import fr.uvsq.projetfrise.outils.ExceptionFrise;
import fr.uvsq.projetfrise.outils.LectureEcriture;
import fr.uvsq.projetfrise.outils.XMLHandler;
import fr.uvsq.projetfrise.outils.XMLUtils;

public class PanelFils extends JPanel implements ActionListener {

    /**
     * @author fcledic & prepain
     * @version 0.1
     */

    private static final long serialVersionUID = 1L;
    private PanelAffichage panelAffichage;
    private PanelCreation panelCreation;

    private Controleur controleur;
    private CardLayout gestionnaire;
    private Frise frise;
    private File fichier;

    public PanelFils(File f) throws ExceptionDate, ExceptionFrise {
        this.gestionnaire = new CardLayout(5, 5);
        this.setLayout(gestionnaire);
        this.fichier=f;

        if (f != null) {
        	this.frise = XMLHandler.frise;
            //frise = (Frise) LectureEcriture.lecture(fichier);// lecture du fichier contenant la frise
            System.out.println("LECTURE DE LA FRISE : ");// debug
            System.out.println("nb : " + frise.getListeEvents().size()+ "\n");
        }else{
//            Evenement e = new Evenement("naissance", new Date(26, 5, 1936), "ahaha", Importance.FAIBLE, "C:\\Users\\paul\\Desktop\\Pixel_art\\logo2.jpg");
//            Evenement e2 = new Evenement("a", new Date(6, 2, 1906), "az", Importance.IMPORTANT, "C:\\Users\\paul\\Desktop\\Pixel_art\\logo4.jpg");
//            Evenement e3 = new Evenement("b", new Date(9, 4, 1940), "ae", Importance.MOYEN, "C:\\Users\\paul\\Desktop\\Pixel_art\\logo3.jpg");
//            TreeSet<Evenement> el = new TreeSet<Evenement>();
//            el.add(e);
//            el.add(e2);
//            el.add(e3);
//            frise = new Frise("Coppola", 1903, 1950, 5, el);
//            System.out.println("FRISE : "+frise);
        }
        this.panelCreation = new PanelCreation(f);// creation de la frise et de ses evenements
        
        if(this.frise != null) {
            panelAffichage = new PanelAffichage(this.frise);// affiche les evenements
            FenetreMere.fenetreMere.getJMenuBar().getMenu(0).getMenuComponent(1).setEnabled(true);
            if(this.frise.getListeEvents().size() != 0)
            FenetreMere.fenetreMere.getJMenuBar().getMenu(1).setEnabled(true);
        }else{
        	FenetreMere.fenetreMere.getJMenuBar().getMenu(0).getMenuComponent(1).setEnabled(false);
        	FenetreMere.fenetreMere.getJMenuBar().getMenu(1).setEnabled(false);
        }
        this.controleur = new Controleur(this, this.frise);

        this.add(panelCreation, Data.TEXTE_MENU_ITEM[0]);
        if(this.frise != null) {
            this.add(panelAffichage, Data.TEXTE_MENU_ITEM[1]);
        }

        
		if(f == null) {
			gestionnaire.show(this, Data.TEXTE_MENU_ITEM[0]);
		}else{
			gestionnaire.show(this, Data.TEXTE_MENU_ITEM[1]);
		}
        
    }
    
    public void loadFrise(Frise f, File fichier) {
    	
    	frise = f;
    	//System.out.println("nb : " + frise.getListeEvents().size()+ "\n");
    	
    	this.fichier = fichier;
    	this.panelAffichage = new PanelAffichage(frise);// affiche les evenements
    	FenetreMere.fenetreMere.getJMenuBar().getMenu(0).getMenuComponent(1).setEnabled(true);
    	FenetreMere.fenetreMere.getJMenuBar().getMenu(1).setEnabled(true);
    	this.add(panelAffichage, Data.TEXTE_MENU_ITEM[1]);
    	this.gestionnaire.show(this, Data.TEXTE_MENU_ITEM[1]);
        this.controleur.setParameter(this, f, fichier);
    }

    public PanelAffichage getPanelAffichage() {
        return panelAffichage;
    }

    public void setPanelAffichage(PanelAffichage panelAffichage) {
        this.panelAffichage = panelAffichage;
    }

    public PanelCreation getPanelCreation() {
        return panelCreation;
    }

    public void setPanelCreation(PanelCreation panelCreation) {
        this.panelCreation = panelCreation;
    }

    public Frise getFrise() {
        return frise;
    }

    public void setFrise(Frise frise) {
        this.frise = frise;
    }

    public Controleur getControleur() {
        return controleur;
    }

    public void actionPerformed(ActionEvent event) {
    	//System.out.println("test");
        if (event.getActionCommand().equals(Data.TEXTE_MENU_ITEM[2])) {
            int saisie = JOptionPane.showConfirmDialog(this, "Voulez-vous quitter l'application ?", "Quitter ?", JOptionPane.YES_NO_OPTION);
            if (saisie == JOptionPane.YES_OPTION) {
            	System.exit(1);
            }
            return;
        }
        
        if(event.getActionCommand().equals(Data.TEXTE_MENU_ITEM[0])) {
        	FenetreMere.fenetreMere.getJMenuBar().getMenu(1).setEnabled(false);
        	this.gestionnaire.show(this, event.getActionCommand());
        }else if(event.getActionCommand().equals(Data.TEXTE_MENU_ITEM[1])){
        	FenetreMere.fenetreMere.getJMenuBar().getMenu(1).setEnabled(true);
        	this.gestionnaire.show(this, event.getActionCommand());
        }else if(event.getActionCommand().equals(Data.TEXTE_MENU_EVENT[0])){//Modification d'un �v�nement
        	
        	//System.out.println("nb : " + this.getFrise().getListeEvents().size()+ "\n");
        	
        	//System.out.println("Ev actuel : " + this.panelAffichage.getPanelEvent().getEventActuel());
        	
        	
        	Evenement e = (Evenement) this.getFrise().getListeEvents().toArray()[
        	          this.panelAffichage.getPanelEvent().getEventActuel()];
        	
        	 System.out.println("Eventafficher:"+e);
        	
        	Evenement eventBase = e;
        	
        	//System.out.println(e);
        	
        	String s = (String)JOptionPane.showInputDialog(this
        	, "Titre : ", "Modifier un Evenement", JOptionPane.PLAIN_MESSAGE, null, null, e.getTitre());

        	System.out.println(s);
        	
        	if ((s != null) && (s.length() > 0)) {
        	    e.setTitre(s);
        	}
        	
        	s = (String)JOptionPane.showInputDialog(this
        	, "Descritpion : ", "Modifier un Evenement", JOptionPane.PLAIN_MESSAGE, null, null, e.getDesc());

        	if ((s != null) && (s.length() > 0)) {
        	    e.setDesc(s);
        	}
        	
        	s = (String)JOptionPane.showInputDialog(this
        	, "Date : ", "Modifier un Evenement", JOptionPane.PLAIN_MESSAGE, null, null, e.getDate().ecrireDate());

        	if ((s != null) && (s.length() > 0)) {
        		Date d;
				try {
					d = Date.lireDate(s);
	        	    e.setDate(d);
				} catch (ExceptionDate e1) {
					e1.printStackTrace();
				}
        	}
        	
        	Importance importance;
        	importance = (Importance)JOptionPane.showInputDialog(this
        	, "Importance : ", "Modifier un Evenement", JOptionPane.PLAIN_MESSAGE, null, Importance.values(), e.getImportance());

        	if ((importance != null)) {
        		e.setImportance(importance);
        	}
        	
        	
        	int i;
        	i = JOptionPane.showConfirmDialog(null, "Changer l'image ?", "Modifier un Evenement", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
        	
        	if (i == JOptionPane.YES_OPTION) {
        		File image = LectureEcriture.getImage(null);
        		if(image != null && ( LectureEcriture.getExtension(image) == "png" || LectureEcriture.getExtension(image) == "jpg")) {
            		e.setCheminPhoto(image.getAbsolutePath());
        		}
        	}
        	
            i = JOptionPane.showConfirmDialog(this, "Confirmer la Modification de l'Evenement", "Confirmation", JOptionPane.YES_NO_OPTION);
            if (i == JOptionPane.YES_OPTION) {
            	
            	System.out.println(this.panelAffichage.getPanelEvent().getEventActuel());
            	
                //System.out.println(this.getFrise().getListeEvents().);
                
            	frise.getListeEvents().remove(eventBase);
            	
            	
            	frise.ajout(e);
            	
                XMLUtils.XMLEcriture(fichier, frise);// enregistrement de la frise dans le fichier
                panelAffichage.getPanelFrise().setFrise(frise);// modification de la JTable
                panelAffichage.getPanelEvent().resetEvenement();
                this.loadFrise(frise, fichier);
            	System.out.println(frise);
            }
        	
        	return;
        	
        }else if(event.getActionCommand().equals(Data.TEXTE_MENU_EVENT[1])){
            int i = JOptionPane.showConfirmDialog(this, "Confirmer la Suppression de l'Evenement", "Confirmation", JOptionPane.YES_NO_OPTION);
            if (i == JOptionPane.YES_OPTION) {
            	Evenement e = (Evenement) this.getFrise().getListeEvents().toArray()[
            	                                                       	          this.panelAffichage.getPanelEvent().getEventActuel()];
            	frise.getListeEvents().remove(e);
            	
                XMLUtils.XMLEcriture(fichier, frise);// enregistrement de la frise dans le fichier
                panelAffichage.getPanelFrise().setFrise(frise);// modification de la JTable
                panelAffichage.getPanelEvent().resetEvenement();
                this.loadFrise(frise, fichier);
            	System.out.println(frise);
            }
        }
        
    }

}
