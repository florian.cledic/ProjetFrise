package fr.uvsq.projetfrise.vue;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import fr.uvsq.projetfrise.modele.Frise;

public class PanelAffichage extends JPanel {

    /**
     * @author fcledic & prepain
     * @version 0.1
     */

    private static final long serialVersionUID = 1L;

    private JLabel titreFrise;
    private PanelEvenement panelEvent;// description d'un evenement
    private PanelFrise panelFrise;// la ou s'affichera la JTable avec tous les evenements

    JScrollPane scrollFrise;
    private BorderLayout bl;

    public PanelAffichage(Frise parFrise) {
        bl = new BorderLayout(5, 5);
        this.setLayout(bl);
        this.titreFrise = new JLabel(parFrise.getTitre(), JLabel.CENTER);
        this.titreFrise.setFont(new Font("Calibri", Font.BOLD, 40));
        this.panelEvent = new PanelEvenement(parFrise);
        this.panelFrise = new PanelFrise(parFrise);
        scrollFrise = new JScrollPane(panelFrise, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        this.add(this.titreFrise, BorderLayout.NORTH);
        this.add(this.panelEvent, BorderLayout.CENTER);
        this.add(this.scrollFrise, BorderLayout.SOUTH);
    }

    public PanelEvenement getPanelEvent() {
        return panelEvent;
    }

    public void setPanelEvent(PanelEvenement panelEvent) {
        this.panelEvent = panelEvent;
    }

    public PanelFrise getPanelFrise() {
        return panelFrise;
    }

    public void setPanelFrise(PanelFrise panelFrise) {
        this.panelFrise = panelFrise;
    }

    public JLabel getTitreFrise() {
        return titreFrise;
    }

    public void setTitreFrise(JLabel titreFrise) {
        this.titreFrise = titreFrise;
    }

}
