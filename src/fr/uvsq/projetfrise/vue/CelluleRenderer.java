package fr.uvsq.projetfrise.vue;

import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CelluleRenderer extends JLabel implements TableCellRenderer {

    private static final long serialVersionUID = 7614980041275818704L;

    public CelluleRenderer() {
        super();
        setOpaque(true);
        setHorizontalAlignment(JLabel.CENTER);
    }

    public Component getTableCellRendererComponent(JTable table, final Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setToolTipText((String) value);
        setIcon(new ImageIcon((String) value));
        return this;
    }

}
