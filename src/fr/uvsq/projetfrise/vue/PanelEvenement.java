package fr.uvsq.projetfrise.vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Frise;
import fr.uvsq.projetfrise.outils.Data;

public class PanelEvenement extends JPanel implements ActionListener {

    /**
     * @author fcledic & prepain
     * @version 0.1
     */
    private static final long serialVersionUID = 1L;

    private CardLayout cd;
    private JButton precedent;
    private JButton suivant;
    private JPanel centre;
    private int eventActuel;
    private Frise frise;

    public PanelEvenement(Frise parFrise) {

        // =-=-=-= PanelCentre =-=-=-=
        // image, titre, date et texte de l'evenement

        this.frise = parFrise;
        centre = new JPanel();
        cd = new CardLayout();
        centre.setLayout(cd);
        eventActuel = 0;
        Collection<Evenement> evts = frise.getListeEvents();
        if (evts != null) {
            Iterator<Evenement> iter = evts.iterator();
            while (iter.hasNext()) {
                ajoutEvenement(iter.next());
            }
        }

        //=-=-=-= Les boutons pour parcourir les evenements =-=-=-=

        precedent = new JButton(Data.TEXTE_BOUTONS[0]);
        suivant = new JButton(Data.TEXTE_BOUTONS[1]);

        precedent.addActionListener(this);
        suivant.addActionListener(this);

        BorderLayout bl = new BorderLayout(5, 5);

        this.setLayout(bl);
        this.add(centre, BorderLayout.CENTER);
        this.add(precedent, BorderLayout.WEST);
        this.add(suivant, BorderLayout.EAST);
    }
    
    public void ajoutEvenement(Evenement evt) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1,2, 5,5));
        
        JLabel image = new JLabel(new ImageIcon(evt.getCheminPhoto()));
        panel.add(image);
        panel.add(new JLabel(evt.toHTMLFormat()));
        centre.add(panel, evt.getTitre());
    }// ajoutEvenement
    
    public void resetEvenement() {
        this.centre.removeAll();
        this.centre.revalidate();
        this.centre.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource().equals(precedent)) {
            if(eventActuel==0) {
                cd.last(this.centre);
                eventActuel = frise.getListeEvents().size();
            }
            else
                cd.previous(this.centre);
            eventActuel--;
        }
        else if (event.getSource().equals(suivant)) {
            if(eventActuel==frise.getListeEvents().size()) {
                cd.first(this.centre);
                eventActuel = -1;
            }
            cd.next(this.centre);
            eventActuel++;
        }

    }// actionPerformed

	public int getEventActuel() {
		return eventActuel;
	}

}
