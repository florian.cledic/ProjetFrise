package fr.uvsq.projetfrise.vue;

import java.io.File;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import fr.uvsq.projetfrise.outils.Data;
import fr.uvsq.projetfrise.outils.ExceptionDate;
import fr.uvsq.projetfrise.outils.ExceptionFrise;
import fr.uvsq.projetfrise.outils.LectureEcriture;
import fr.uvsq.projetfrise.outils.XMLHandler;
import fr.uvsq.projetfrise.outils.XMLUtils;

public class FenetreMere extends JFrame {

    /**
     * @author fcledic & prepain
     * @version 0.1
     */

    private static final long serialVersionUID = 1L;
    private PanelFils panelFils;
    public static FenetreMere fenetreMere;

    public FenetreMere(File f) throws ExceptionDate, ExceptionFrise {
        super("Frise Visualiseur");
        fenetreMere = this;

        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);

        JMenu menu = new JMenu("Menu");
        menu.setMnemonic('M');
        for (String item : Data.TEXTE_MENU_ITEM) {
        	JMenuItem itemMenu = new JMenuItem(item, item.charAt(0));
            menu.add(itemMenu);
        }
        menuBar.add(menu);
        
        JMenu menuEvent = new JMenu("Evenement");
        menuEvent.setMnemonic('E');
        for (String item : Data.TEXTE_MENU_EVENT) {
        	JMenuItem itemMenu = new JMenuItem(item, item.charAt(0));
        	menuEvent.add(itemMenu);
        }
        menuBar.add(menuEvent);
        
        this.panelFils = new PanelFils(f);
        
        int i = 0;
        for (String item : Data.TEXTE_MENU_ITEM) {
        	JMenuItem itemMenu = this.getJMenuBar().getMenu(0).getItem(i);
        	itemMenu.addActionListener(this.panelFils);
            itemMenu.setActionCommand(item);
            i++;
        }
        
        i = 0;
        for (String item : Data.TEXTE_MENU_EVENT) {
        	JMenuItem itemMenu = this.getJMenuBar().getMenu(1).getItem(i);
        	itemMenu.addActionListener(this.panelFils);
            itemMenu.setActionCommand(item);
            i++;
        }

        setContentPane(this.panelFils);

        //LectureEcriture.getFile(this.getContentPane());

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1300, 700);
        setVisible(true);
    }

    public static void main(String[] args) {

        //File save = LectureEcriture.saveFile(null);
        //XMLUtils.XMLEcriture(save);
        //File f1 = LectureEcriture.getFile(null);
        //XMLUtils.XMLLecture(f1);
        //System.out.println("Lecture de la Frise");
        //System.out.println(XMLHandler.frise);

        Object[] options = { "Cr�er une Frise", "Ouvrir un fichier .ser" };
        int option = JOptionPane.showOptionDialog(null, "S�lectionner une option", "Frise", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

        File f = null;
        //System.out.println(option);

        if (options[option] == options[1]) { // Ouvrir une frise
            System.out.println("Ouvrir une frise");
            f = LectureEcriture.getFile(null);
            XMLUtils.XMLLecture(f);
            System.out.println("Lecture de la Frise");
            System.out.println(XMLHandler.frise);
        }
        else {

        }

        try {
            new FenetreMere(f);
        }
        catch (ExceptionDate | ExceptionFrise e) {
            e.printStackTrace();
        }
    }

	public PanelFils getPanelFils() {
		return panelFils;
	}

}
