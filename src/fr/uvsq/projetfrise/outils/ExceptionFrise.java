package fr.uvsq.projetfrise.outils;

public class ExceptionFrise extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ExceptionFrise(String msg) {
        super(msg);
    }

}
