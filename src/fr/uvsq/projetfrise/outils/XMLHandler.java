package fr.uvsq.projetfrise.outils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import fr.uvsq.projetfrise.modele.Date;
import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Frise;
import fr.uvsq.projetfrise.modele.Importance;

public class XMLHandler extends DefaultHandler {

    public static Frise frise;

    private String node = null;
    private Attributes attribut;

    private boolean options = false;
    private String titre;
    private int dateDebut;
    private int dateFin;
    private int periode;

    private String dateEvent;
    private int importance;
    private String cheminImage;
    private String desc;

    private boolean flipFlopDonn�e = false;

    // d�but du parsing
    public void startDocument() throws SAXException {
        System.out.println("D�but du parsing");

    }

    // fin du parsing
    public void endDocument() throws SAXException {
        System.out.println("Fin du parsing");
    }

    public void startElement(String namespaceURI, String lname, String qname, Attributes attrs) throws SAXException {

        //System.out.println("---------------------------------------------");

        // cette variable contient le nom du n�ud qui a cr�� l'�v�nement

        //System.out.println("qname = " + qname);

        node = qname;

        // Cette derni�re contient la liste des attributs du n�ud

        if (attrs != null) {

            for (int i = 0; i < attrs.getLength(); i++) {

                // nous r�cup�rons le nom de l'attribut

                String aname = attrs.getLocalName(i);

                // Et nous affichons sa valeur

                //System.out.println("Attribut " + aname + " valeur : " + attrs.getValue(i));

            }

        }

        if (node == "Options") {
            options = true;
        }

        if (node == "Evenement") {
            if (attrs.getLocalName(0) == "Date") {
                this.dateEvent = attrs.getValue(0);
            }
            if (attrs.getLocalName(1) == "Importance") {
                this.importance = Integer.parseInt(attrs.getValue(1));
            }
        }

        this.attribut = attrs;

    }

    public void endElement(String uri, String localName, String qName) throws SAXException {

        //System.out.println("Fin de l'�l�ment " + qName);

        if (qName == "Options") {
            this.options = false;
            try {
                frise = new Frise(this.titre, this.dateDebut, this.dateFin, this.periode);
            }
            catch (ExceptionFrise e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println("frise cr�e");
        }
        else if (qName == "Evenement") {
            try {
                Evenement event = new Evenement(this.titre, Date.lireDate(this.dateEvent), this.desc, Importance.values()[this.importance], this.cheminImage);
                System.out.println("Event : " + event);
                frise.ajout(event);
                System.out.println("Evenement Ajout�");
                System.out.println("Total d'Evenement ajout�s : " + frise.getListeEvents().size());
            }
            catch (ExceptionDate e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * permet de r�cup�rer la valeur d'un n�ud
     */
    public void characters(char[] data, int start, int end) {

        if (flipFlopDonn�e == true) {
            flipFlopDonn�e = false;
            return;
        }
        else {
            flipFlopDonn�e = true;
        }

        //System.out.println("***********************************************");
        String donn�e = new String(data, start, end);

        // La variable data contient tout notre fichier.
        // Pour r�cup�rer la valeur, nous devons nous servir des limites en param�tre
        // "start" correspond � l'indice o� commence la valeur recherch�e
        // "end" correspond � la longueur de la cha�ne

        //System.out.println("Donn�e du n�ud " + node + " : " + donn�e);

        if (options == true) { //Option

            switch (node) {
                case "Titre":
                    this.titre = donn�e;
                    break;

                case "Date":
                    //System.out.println(this.attribut.getLocalName(0) + " = " + this.attribut.getValue(0));
                    if (this.attribut.getLocalName(0).equals("periode") && this.attribut.getValue(0).equals("D�but")) {
                    	this.dateDebut = Integer.parseInt(donn�e);
                    }
                    else if (this.attribut.getLocalName(0).equals("periode") && this.attribut.getValue(0).equals("Fin")) {
                        this.dateFin = Integer.parseInt(donn�e);
                    }
                    break;

                case "Periode":
                    this.periode = Integer.parseInt(donn�e);

            }
        }
        else { //Evenement

            switch (node) {
                case "Titre":
                    this.titre = donn�e;
                    break;

                case "CheminImage":
                    this.cheminImage = donn�e;
                    break;

                case "Description":
                    this.desc = donn�e;
            }
        }
    }
}