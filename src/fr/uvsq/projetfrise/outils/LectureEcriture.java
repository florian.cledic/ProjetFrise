package fr.uvsq.projetfrise.outils;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Base64;

import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;

public class LectureEcriture {

    // public static Object lecture(File parFichier) {
    // ObjectInputStream flux;
    // Object objetLu = null;
    // // Ouverture du fichier en mode lecture
    // try {
    // flux = new ObjectInputStream(new FileInputStream(parFichier));
    // objetLu = (Object) flux.readObject();
    // flux.close();
    // } catch (ClassNotFoundException parException) {
    // System.err.println(parException.toString());
    // System.exit(1);
    // } catch (IOException parException) {
    // System.err.println("Erreur lecture du fichier " + parException.toString());
    // System.exit(1);
    // }
    // return objetLu;
    // } // lecture ()

//    public static void ecriture(File parFichier, Object parObjet) {
//        ObjectOutputStream flux = null;
//        // Ouverture du fichier en mode ecriture
//        try {
//            flux = new ObjectOutputStream(new FileOutputStream(parFichier));
//            flux.writeObject(parObjet);
//            flux.flush();
//            flux.close();
//        }
//
//        catch (IOException parException) {
//            System.err.println("Probleme a l'ecriture\n" + parException.toString());
//            System.exit(1);
//        }
//    } // ecriture ()

    public static File getFile(Component parent) {

        JFileChooser fchooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier .ser", "ser");
        fchooser.setFileFilter(filter);

        fchooser.setCurrentDirectory(new File("/"));

        int returnVal = fchooser.showOpenDialog(parent);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You chose to open this file: " + fchooser.getSelectedFile().getName());

            // System.out.println(LectureEcriture.lecture(fchooser.getSelectedFile()));

            return fchooser.getSelectedFile();
        }

        return null;

    }

    public static File saveFile(Component parent) {

        JFileChooser fchooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier .ser", "ser");
        fchooser.setFileFilter(filter);

        fchooser.setCurrentDirectory(new File("/"));

        int returnVal = fchooser.showSaveDialog(parent);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            if (getExtension(fchooser.getSelectedFile()) != null) {

                if (fchooser.getSelectedFile().exists() && getExtension(fchooser.getSelectedFile()) != "ser" && !getExtension(fchooser.getSelectedFile()).contains("ser")) {
                    String chemin = fchooser.getSelectedFile().getAbsolutePath() + ".ser";
                    fchooser.setSelectedFile(new File(chemin));
                }

            }
            else {
                String chemin = fchooser.getSelectedFile().getAbsolutePath() + ".ser";
                fchooser.setSelectedFile(new File(chemin));
            }

            System.out.println("You chose to save this file: " + fchooser.getSelectedFile().getName());

            // System.out.println(LectureEcriture.lecture(fchooser.getSelectedFile()));

            return fchooser.getSelectedFile();
        }

        return null;

    }

    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        //System.out.println(ext);
        //System.out.println("Nom : " + f.getName());
        return ext;
    }

    public static File getImage(Component parent) {

        JFileChooser fchooser = new JFileChooser();
        FileNameExtensionFilter filter0 = new FileNameExtensionFilter("Fichier .png", "png");
        fchooser.setFileFilter(filter0);
        FileNameExtensionFilter filter1 = new FileNameExtensionFilter("Fichier .jpg", "jpg");
        fchooser.setFileFilter(filter1);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier .png et .jpg", "png", "jpg");
        fchooser.setFileFilter(filter);

        fchooser.setCurrentDirectory(new File("/"));

        int returnVal = fchooser.showOpenDialog(parent);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You chose to open this file: " + fchooser.getSelectedFile().getName());

            //System.out.println(LectureEcriture.lecture(fchooser.getSelectedFile()));

            return fchooser.getSelectedFile();
        }

        return null;

    }  

}// LectureEcriture