package fr.uvsq.projetfrise.modele;

import javax.swing.table.DefaultTableModel;

import fr.uvsq.projetfrise.outils.Data;

/**
 * ModeleTable
 * 
 * Modele utilise pour afficher la frise chronologique. Compose de 4 lignes, correspondants au poids d'un evenement, et
 * d'un nombre de colonnes dependant des dates de debut et de fin de la frise (en l'occurence dateDeFin - dateDeDebut + 1).
 * L'en-tete du modele affiche les annees, sur un intervalle, donne par la periode de la frise. 
 * 
 * 
 * 
 * @author paul
 *
 */
public class ModeleTable extends DefaultTableModel {

    private static final long serialVersionUID = 1L;
    Frise frise;

    ///////////////////////
    //                   //
    //   CONSTRUCTEURS   //
    //                   //
    ///////////////////////

    public ModeleTable(Frise parFrise) {
        this.frise = parFrise;
        this.setColumnCount(frise.getDateFin() - frise.getDateDebut() + 1);// nombre de colonnes (nombre d'annees)
        this.setRowCount(Data.IMPORTANCE_MAX);// poids
        if (frise.getListeEvents() != null) {
            for (Evenement e : frise.getListeEvents())
                ajoutEvenement(e);
        }
        //System.out.println(frise.getPeriode());
        this.entete(frise.getDateDebut(), frise.getDateFin(), frise.getPeriode());

    }// ModeleTable()

    //////////////////////
    //                  //
    //     METHODES     //
    //                  //
    //////////////////////

    public void ajoutEvenement(Evenement e) {
    	
    	System.out.println(e.getDate().getAnnee() - frise.getDateDebut());
    	
        setValueAt(e.getCheminPhoto(), e.getImportance().ordinal(), e.getDate().getAnnee() - frise.getDateDebut());
    }// ajoutEvenement()

    public void entete(int dateDebut, int dateFin, int periode) {
        String annees[] = new String[dateFin - dateDebut + 1];
        //System.out.println(periode);
        for (int annee = 0; annee <= dateFin - dateDebut; annee++) {
            if (periode == 0 || annee % periode == 0)
                annees[annee] = Integer.toString(annee + dateDebut);
            else
                annees[annee] = new String();
        }
        this.setColumnIdentifiers(annees);
    }

    public boolean isCellEditable(int indiceLig, int indiceCol) {
        return false;// ?
    }// isCellEditable()

    public Class<Evenement> getColumnClass(int parNum) {
        return Evenement.class;
    }// getColumnClass()
}
