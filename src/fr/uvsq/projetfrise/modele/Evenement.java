package fr.uvsq.projetfrise.modele;

import java.io.Serializable;

/**
 * Evenement
 * 
 * Identifi� par une date, un titre, une description, un poids et du chemin vers une photo correspondant � l'�v�nement
 * 
 * @author fcledic & paul
 *
 */
public class Evenement implements Comparable<Evenement>, Serializable {

    private static final long serialVersionUID = 1L;
    private Date date;// annee
    private String titre;
    private String desc;
    private Importance importance;
    private String cheminPhoto;

    /**
     * Constructeur d'Evenement. Prend en compte tous les champs de la classe.
     * @param titre
     * @param date
     * @param desc
     * @param importance
     * @param cheminPhoto
     */
    public Evenement(String titre, Date date, String desc, Importance importance, String cheminPhoto) {
        this.titre = titre;
        this.date = date;
        this.desc = desc;
        this.setImportance(importance);
        this.setCheminPhoto(cheminPhoto);
    }

    /**
     * Constructeur d'Evenement. Ne prend pas en compte le chemin de la photo.
     * @param titre
     * @param date
     * @param desc
     * @param importance
     */
    public Evenement(String titre, Date date, String desc, Importance importance) {
        this.titre = titre;
        this.date = date;
        this.desc = desc;
        this.setImportance(importance);
    }

    /**
     * Retourne une chaine de caractere decrivant l'evenement
     * @return chaine de caracteres decrivant l'evenement :
     * titre, description, date et le chemin de la photo dans l'ordinateur
     */
    public String toString() {
        return "Titre : " + titre + "\nDescription : " + desc + "\nDate : " + date.toString2() + "\nChemin de la photo : " + cheminPhoto;
    }

    /**
     * Mise en forme html de l'evenement.
     * @return une chaine de caracteres incluse dans un bloc html
     * @see StringBuilder#append(String)
     */
    public String toHTMLFormat() {
        StringBuilder HTML = new StringBuilder();

        HTML.append("<html>");
        HTML.append("<h4><i>");
        HTML.append(this.date.toString2());
        HTML.append("</i></h4>");
        HTML.append("<h3>");
        HTML.append(this.titre);
        HTML.append("</h3>");
        HTML.append(this.desc);
        HTML.append("</html>");

        return HTML.toString();
    }

    /**
     * Accesseur du chemin de la photo de l'evenement
     * @return le chemin de la photo dans l'ordinateur de l'utilisateur
     */
    public String getCheminPhoto() {
        return cheminPhoto;
    }

    /**
     * Modifie le chemin de la photo de l'evenement
     * @param cheminPhoto
     */
    public void setCheminPhoto(String cheminPhoto) {
        this.cheminPhoto = cheminPhoto;
    }

    /**
     * Accesseur de la date de l'evenement
     * @return La date de l'evenement
     */
    public Date getDate() {
        return date;
    }

    /**
     * Modifieur : modifie la date de l'evenement
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Accesseur : retourne le titre de l'evenement
     * @return Le titre de l'evenement
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Modifieur : modifie le titre de l'evenement
     * @param titre
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Accesseur : retourne la description de l'evenement
     * @return La description de l'evenement
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Modifieur : modifie la description de l'evenement
     * @param desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * Accesseur : retourne le poids de l'evenement
     * @return Le poids l'evenement
     */
    public Importance getImportance() {
        return importance;
    }

    /**
     * Modifieur : modifie le poids de l'evenement
     * @param importance
     */
    public void setImportance(Importance importance) {
        this.importance = importance;
    }

    /**
     * M�thode permettant de ranger les objets dans un TreeSet d'Evenement
     * @see Frise
     * @return Un entier, n�gatif si l'evenement courant est inf�rieur � l'evenement pass� en param�tre
     * Compare dans l'ordre :
     * date
     * titre
     * description
     * importance
     */
    public int compareTo(Evenement o) {
        if(this.date.compareTo(o.date)!=0)
            return this.date.compareTo(o.date);
        else {
            if(this.titre.compareTo(o.titre)!=0)
                return this.titre.compareTo(o.titre);
            else {
                if(this.desc.compareTo(o.desc)!=0)
                    return this.desc.compareTo(o.desc);
                else {
                    if(this.importance.ordinal()>o.importance.ordinal())
                        return -1;
                    else if(this.importance.ordinal()<o.importance.ordinal())
                        return 1;
                }
            }
        }
        return 0;
    }// compareTo

}
