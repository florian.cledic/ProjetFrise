package fr.uvsq.projetfrise.controleur;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JOptionPane;

import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Frise;
import fr.uvsq.projetfrise.outils.Data;
import fr.uvsq.projetfrise.outils.ExceptionDate;
import fr.uvsq.projetfrise.outils.ExceptionFrise;
import fr.uvsq.projetfrise.outils.LectureEcriture;
import fr.uvsq.projetfrise.outils.XMLUtils;
import fr.uvsq.projetfrise.vue.PanelAffichage;
import fr.uvsq.projetfrise.vue.PanelCreation;
import fr.uvsq.projetfrise.vue.PanelFils;

/**
 * Controleur
 * 
 * Objet se mettant a l'ecoute des differents JPanel qui censes communiquer les uns avec les autres.
 * Autrement dit, le controleur gere les evenements entre les classes
 * 
 * 
 * @author fcledic & paul
 *
 */
public class Controleur implements ActionListener {

	private PanelFils panelFils;
    private PanelCreation panelCreation;
    private PanelAffichage panelAffichage;
    private Frise frise;
    File fichier;

    /**
     * Constructeur du Controleur.
     * @param panelFils
     * @param frise
     */
    public Controleur(PanelFils panelFils, Frise frise) {
    	this.panelFils = panelFils;
        this.panelCreation = panelFils.getPanelCreation();
        this.panelAffichage = panelFils.getPanelAffichage();
        panelCreation.enregistreEcouteur(this);
        this.frise = frise;
        fichier = new File("FileFrise" + File.separator + "frise.ser");
    }
    
    public Controleur(PanelFils panelFils, Frise frise, File fichier) {
    	this.panelFils = panelFils;
        this.panelCreation = panelFils.getPanelCreation();
        this.panelAffichage = panelFils.getPanelAffichage();
        panelCreation.enregistreEcouteur(this);
        this.frise = frise;
        this.fichier = fichier;
    }
    
    public void setParameter(PanelFils panelFils, Frise frise,  File fichier){
    	this.panelFils = panelFils;
        this.panelAffichage = panelFils.getPanelAffichage();
        this.frise = frise;
        this.fichier = fichier;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(Data.INTITULE_CREER_EVENT)) {
            Evenement evt;
            try {
                evt = panelCreation.getEvenement();
                if(evt.getDate().getAnnee() > frise.getDateFin() || evt.getDate().getAnnee() < frise.getDateDebut()) {
                	JOptionPane.showMessageDialog(null, "Ann�e incorrecte", "Erreur", JOptionPane.ERROR_MESSAGE, null);
                	return;
                }
                JOptionPane.showMessageDialog((Component) e.getSource(), evt.toString());
                frise.ajout(evt);// ajout d'un evenement a la frise
                //System.out.println(frise);
                XMLUtils.XMLEcriture(this.fichier, frise);
                //LectureEcriture.ecriture(fichier, frise);
                panelAffichage.getPanelFrise().ajoutEvenement(evt);
                panelAffichage.getPanelEvent().ajoutEvenement(evt);
                panelCreation.reset();// les champs du formulaire sont r�initialis�s
            }
            catch (ExceptionDate e1) {
                e1.printStackTrace();
            }

        }
        else if (e.getActionCommand().equals(Data.INTITULE_CREER_FRISE)) {
            try {
                frise = panelCreation.getFrise();
            }
            catch (ExceptionFrise e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            
            if(frise.getDateFin() < frise.getDateDebut()) {
            	JOptionPane.showMessageDialog(null, "La date de fin est inferieur � la date de d�but", "Erreur", JOptionPane.ERROR_MESSAGE, null);
            	return;
            }
            
            fichier = LectureEcriture.saveFile(null);
            XMLUtils.XMLEcriture(fichier, frise);// enregistrement de la frise dans le fichier
            panelFils.loadFrise(frise, fichier);
            panelAffichage.getPanelFrise().setFrise(frise);// modification de la JTable
            panelAffichage.getTitreFrise().setText(frise.getTitre());// changement du titre de la frise sur le PanelAffichage
            panelAffichage.getPanelEvent().resetEvenement();
            panelCreation.reset();// les champs du formulaire sont r�initialis�s
        }
    }

}
