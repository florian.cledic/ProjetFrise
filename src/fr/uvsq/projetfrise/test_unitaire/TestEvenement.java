package fr.uvsq.projetfrise.test_unitaire;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.uvsq.projetfrise.modele.Date;
import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Importance;
import fr.uvsq.projetfrise.outils.ExceptionDate;

public class TestEvenement {

    @Test
    public void testEvenementStringDateStringImportanceString() {
        try {
            Evenement e = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE, "C:\\Users\\paul\\Desktop\\Pixel_art\\logo3.jpg");
        }
        catch (ExceptionDate e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testEvenementStringDateStringImportance() {
        try {
            Evenement e = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE);
        }
        catch (ExceptionDate e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testToString() {
        try {
            Evenement e = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE, "C:\\Users\\paul\\Desktop\\Pixel_art\\logo3.jpg");
            String estr = new String("Titre : titre\nDescription : description\nDate : 3 avril 2000\nChemin de la photo : C:\\Users\\paul\\Desktop\\Pixel_art\\logo3.jpg");
            
            assertEquals("Test TOSTRING", 0, e.toString().compareTo(estr));

        }
        catch (ExceptionDate e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testToHTMLFormat() {
        try {
            Evenement e = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE);
            String estr = new String("<html><h4><i>3 avril 2000</i></h4><h3>titre</h3>description</html>");
            assertEquals("Test TOHTMLFORMAT", 0, e.toHTMLFormat().compareTo(estr));
        }
        catch (ExceptionDate e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }



    @Test
    public void testCompareTo() {
        try {
            Evenement e = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE);
            Evenement e2 = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE);
            assertEquals("Test COMPARETO", 0, e.compareTo(e2));
            Evenement e3 = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE);
            Evenement e4 = new Evenement("titre", new Date(3,4,2000), "description", Importance.MOYEN);
            assertEquals("Test COMPARETO", -1, e3.compareTo(e4));
            assertEquals("Test COMPARETO", 1, e4.compareTo(e3));
            Evenement e5 = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE);
            Evenement e6 = new Evenement("titre", new Date(3,4,1009), "description", Importance.FAIBLE);
            assertEquals("Test COMPARETO", 1, e5.compareTo(e6));
            assertEquals("Test COMPARETO", -1, e6.compareTo(e5));
            
        }
        catch (ExceptionDate e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

}
