package fr.uvsq.projetfrise.test_unitaire;

import static org.junit.Assert.*;

import java.util.TreeSet;

import org.junit.Test;

import fr.uvsq.projetfrise.modele.Date;
import fr.uvsq.projetfrise.modele.Evenement;
import fr.uvsq.projetfrise.modele.Frise;
import fr.uvsq.projetfrise.modele.Importance;
import fr.uvsq.projetfrise.outils.ExceptionDate;
import fr.uvsq.projetfrise.outils.ExceptionFrise;

public class TestFrise {

    @Test
    public void testFriseStringIntIntIntTreeSetOfEvenement() {
        Evenement e1;
        try {
            e1 = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE);
            TreeSet<Evenement> e = new TreeSet<Evenement>();
            e.add(e1);
            Frise f = new Frise("titre", 1900, 1901, 1, e);
        }
        catch (ExceptionDate | ExceptionFrise e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    @Test
    public void testFriseStringIntIntInt() {
        try {
            Frise f = new Frise("titre", 1900, 1901, 1);
        }
        catch (ExceptionFrise e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

    @Test
    public void testToString() {
        try {
            Evenement e1 = new Evenement("titre", new Date(3,4,2000), "description", Importance.FAIBLE, "C:\\Users\\paul\\Desktop\\Pixel_art\\logo3.jpg");
            TreeSet<Evenement> e = new TreeSet<Evenement>();
            e.add(e1);
            Frise f = new Frise("titre", 1900, 1901, 1, e);
            String fstr=new String("Titre : titre\nDate : de 1900 a 1901\nListe des evenements (1) : \n");
            fstr+="Titre : titre\nDescription : description\nDate : 3 avril 2000\nChemin de la photo : C:\\Users\\paul\\Desktop\\Pixel_art\\logo3.jpg";
            assertEquals("Test TOSTRING", 0, f.toString().compareTo(fstr));
        }
        catch (ExceptionDate | ExceptionFrise e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
    }

}
